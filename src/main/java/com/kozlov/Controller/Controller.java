package com.kozlov.Controller;
import com.kozlov.Model.Minesweeper.Minesweeper;
import com.kozlov.Model.plateau.Plateau;

import java.util.ArrayList;
import java.util.List;

public class Controller {
    Minesweeper model = new Minesweeper();
    Plateau plateu = new Plateau(new ArrayList<>());
public void minesGame(int width, int height, double probability){
    model.minesGameBasic(width, height, probability);
}
public int getLongestSequence(List<Integer> list){
     return plateu.longestLength();
}
public String getStartEnd(){
    return plateu.getRange();
}
}
