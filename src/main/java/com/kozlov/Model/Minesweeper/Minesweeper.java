package com.kozlov.Model.Minesweeper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.Random;
public class Minesweeper {
    protected static final String BOMB = "*";
    private static Logger logger = LogManager.getLogger(Minesweeper.class);
    private Random random = new Random();
    private int[][] field;
public void minesGameBasic(int width, int height, double probability) throws NegativeArraySizeException{
    createField(width, height, probability);
    printGame(width, height);
    printSolution(width, height);
}
private void createField(int width, int height, double probability){
    field = new int[width + 2][height + 2];
    for(int i = 1; i < width + 1; i ++){
        for(int j = 1; j < height + 1; j++){
            int randNumber = random.nextInt(100);
            if(probability > randNumber){
                field[i][j] = 1;
            }
            else {
                field[i][j] = 0;
            }
            if(field[i][j]==1){
                logger.info(BOMB + " ");
            }
            else {
                logger.info(". ");
            }
        }
        logger.info("\n");
    }
}
private void printGame(int width, int height){
       int bombsNumber;
    for(int i = 1; i < width + 1; i ++){
        for(int j = 1; j < height + 1; j++) {
            bombsNumber = field[i-1][j] + field[i-1][j-1] + field[i-1][j+1] +
            field[i][j-1] + field[i][j] + field[i][j+1] +
            field[i+1][j-1] + field[i+1][j] + field[i+1][j+1];
            logger.info(bombsNumber + " ");
        }
        logger.info("\n");
        }
}
private void printSolution(int width, int height){
    int bombsNumber;
    for(int i = 1; i < width + 1; i ++){
        for(int j = 1; j < height + 1; j++) {
            if(field[i][j] == 0)
            {
                bombsNumber = field[i-1][j] + field[i-1][j-1] + field[i-1][j+1] +
                        field[i][j-1] + field[i][j] + field[i][j+1] +
                        field[i+1][j-1] + field[i+1][j] + field[i+1][j+1];
                if(bombsNumber != 0) {
                    logger.info(bombsNumber + " ");
                }
                else {
                    logger.info( ". ");
                }
            }
            else {
                logger.info("■" + " ");
            }
        }
        logger.info("\n");
    }
}
public String getBomb(){
    return BOMB;
}
}
