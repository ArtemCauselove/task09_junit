package com.kozlov.Model.plateau;

import java.util.ArrayList;
import java.util.List;

public class Plateau {
    private List<Integer> numbersList;
    private int startRange;
    private int endRange;

    public Plateau(List<Integer> numbers) {
        numbersList = new ArrayList<>(numbers);
    }

    public int longestLength() {
        List<Integer> longestSequences = new ArrayList<>();
        List<Integer> currentSequence;
        for (int i = 0; i < numbersList.size() - 1; i++) {
            if ((numbersList.get(i).equals(numbersList.get(i + 1)))) {
                currentSequence = getSequence(numbersList.get(i), numbersList.subList(i, numbersList.size()));
                if (inCorrectRange(i, i + currentSequence.size() - 1)) {
                    if (currentSequence.size() > longestSequences.size()) {
                        longestSequences = new ArrayList<>(currentSequence);
                        startRange = i;
                        endRange = i + longestSequences.size();
                    }
                }
                i += currentSequence.size();
            }
        }
        return longestSequences.size();
    }

    public String getRange() {
        return "Start at: " + startRange + " End at: " + endRange;
    }

    public List<Integer> getSequence(int value, List<Integer> sequence) {
        List<Integer> equalSequence = new ArrayList<>();
        for (int currentValue : sequence) {
            if (value == currentValue) {
                equalSequence.add(currentValue);
            } else {
                break;
            }
        }
        return equalSequence;
    }

    public boolean inCorrectRange(int start, int end) {
        if (start - 1 >= 0 && end + 1 < numbersList.size()) {
            return numbersList.get(start) > numbersList.get(start - 1);
        }
        return true;
    }
}