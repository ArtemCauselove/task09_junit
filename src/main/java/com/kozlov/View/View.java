package com.kozlov.View;
import com.kozlov.Controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class View {
    private Map<Integer, String> menu = new HashMap<>();
    private Map<String, Printable> methodsMenu = new HashMap<>();
    private Controller controller = new Controller();
    private Scanner scan = new Scanner(System.in, "UTF-8");
    private static Logger logger = LogManager.getLogger(View.class);
    public View() {
        menu.put(0, "Minesweeper Game.\n");
        menu.put(1, "Length and range of longest sequence. \n");
        menu.put(2, "Exit. \n");
        methodsMenu.put("0", this::MinesweeperGame);
        methodsMenu.put("1", this::Sequence);
        methodsMenu.put("2", this::quit);
    }
    public void show() {
         String keyMenu;
        do {
            showInfo();
            logger.info("Please, select menu point.\n");
            keyMenu = scan.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
                logger.info("");
            } catch (Exception e) {
                e.printStackTrace();
            }
        } while (!keyMenu.equals("Q"));
    }
    private void quit() {
        logger.info("Exit\n");}
    private void showInfo() {
        logger.info("Menu for TextTask\n");
        menu.forEach((key, elem) -> logger.info(key + " : " + elem));
    }
    private void MinesweeperGame(){
        logger.info("Input width and height for the fields");
        int width = scan.nextInt();
        int height = scan.nextInt();
        logger.info("Input bombs probability");
        double propability = scan.nextDouble();
        controller.minesGame(width, height, propability);
    }
    private void Sequence(){
        List<Integer> userNumbers = new ArrayList<>();
        logger.info("Print numbers\n");
        String currentNumber;
        while(true){
            currentNumber = scan.nextLine();
            if (currentNumber.isEmpty()) {
                break;
            }
           userNumbers.add(Integer.valueOf(currentNumber));
        }
        logger.info(controller.getLongestSequence(userNumbers) + "\n");
        logger.info(controller.getStartEnd() + "\n");
    }
}

