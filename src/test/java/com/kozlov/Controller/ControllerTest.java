package com.kozlov.Controller;

import com.kozlov.Model.Minesweeper.Minesweeper;
import com.kozlov.Model.plateau.Plateau;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ControllerTest {
    @InjectMocks
    Controller controller;
    //@Mock annotation is used to create the mock object to be injected
    @Mock
    Plateau plateau = new Plateau(new ArrayList<>());
    @Test
    public void testLongest(){
        List<Integer> list = new ArrayList<>();
        list.add(1);
        when(plateau.longestLength()).thenReturn(5);
        //when(main.getLongestSequence(new ArrayList<>())).thenReturn(5);
        System.out.print(controller.getLongestSequence(list));
        assertEquals(controller.getLongestSequence(list),5);
    }
}
